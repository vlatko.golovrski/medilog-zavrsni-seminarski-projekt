""" Sesiomat.py importira Sqlalchemy bibilioteku i stvara sesiju za upravljanje 
podacima na bazi podataka BazaPodataka.db """


from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

'''
@author: Vlatko Golovrški
@version: 1.0
@date of creation: 20.4.2023.

'''

def kreirajSesiju():
    engine = create_engine("sqlite:///Assets/BazaPodataka.db")
    DBSession = sessionmaker(bind=engine)
    sesija = DBSession()
    return sesija