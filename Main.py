"""Main.py je početna Pythom skripta koja poziva sesiju iz sesiomata i pokreće aplikaciju te otvara prvi odnosno login prozor. """


import sys
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from ViewModels.LoginProzor import LoginProzor
from Assets.Sesiomat import kreirajSesiju


'''
@author: Vlatko Golovrški
@version: 1.0
@date of creation: 20.4.2023.

'''

app = QApplication(sys.argv)

sesija = kreirajSesiju()

prozor = LoginProzor(sesija)
prozor.show()

sys.exit(app.exec())


