"""IzmjenaLijecnika.py prikazuje prozor za izmjenu podataka odabranog liječnika. Podatke kojima se popunjavaju polja za izmjenu skripta dobiva
iz PopisLijecnika.py. Korisnik može mijenjati podatke o liječniku a nakon promjene stisnuti SPREMI. Nakon toga pokreće se metoda koja sprema unesene podatke 
uz pomoć sesije u bazu kao i obavijest o uspješnom spremanju izmjena."""


from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5 import uic
from Models.Lijecnik import Lijecnik

'''
@author: Vlatko Golovrški
@version: 1.0
@date of creation: 20.4.2023.

'''


class IzmjenaLijecnika(QWidget):
    data_changed = pyqtSignal() #stvaranje klase signala u PYQt-u kako bi se mogla pozvati iz metode spremi izmjene
    
    def __init__(self,ime, prezime, oib, adresa, datumRodenja, spol, specijalizacija, 
                 regBroj, telefon, email,sesija, povratak):
        super().__init__()
        uic.loadUi("./Views/izmjenaLijecnika.ui",self)
        self.sesija = sesija

        #popunjavanje tablice
        self.ime_text.setText(ime)
        self.prezime_text.setText(prezime)
        self.oib_text.setText(oib)
        self.adresa_text.setText(adresa)
        self.dat_rod_text.setText(datumRodenja)
        self.spol_combo.setCurrentText(spol)
        self.specijalizacija_text.setText(specijalizacija)
        self.regbroj_text.setText(regBroj)
        self.telefon_text.setText(telefon)
        self.e_mail_text.setText(email)

        self.izlazButton.clicked.connect(self.close)
        self.izlazButton.clicked.connect(povratak)
        self.SpremiButton.clicked.connect(self.spremiIzmjene)
        self.SpremiButton.clicked.connect(povratak)
    
    def spremiIzmjene(self):

        ime = self.ime_text.text().strip()
        prezime = self.prezime_text.text().strip()
        oib = self.oib_text.text().strip()
        adresa = self.adresa_text.text().strip()
        datumRodjenja = self.dat_rod_text.text().strip()
        spol = self.spol_combo.currentText()
        specijalizacija = self.specijalizacija_text.text().strip()
        regBroj = self.regbroj_text.text().strip()
        telefon = self.telefon_text.text().strip()
        email = self.e_mail_text.text().strip()
       
        try:
            #pretraživanje liječnika koji se mijanja preko OIB-a
            lijecnik = self.sesija.query(Lijecnik).filter(Lijecnik.OIB == oib).first()

            lijecnik.IME = ime
            lijecnik.PREZIME = prezime
            lijecnik.OIB = oib
            lijecnik.ADRESA = adresa
            lijecnik.DATUMRODENJA = datumRodjenja
            lijecnik.SPOL = spol
            lijecnik.SPECIJALIZACIJA = specijalizacija
            lijecnik.REGBROJ = regBroj
            lijecnik.TELEFON = telefon
            lijecnik.EMAIL = email
                            
            self.sesija.commit()
                                
            self.porukaProzor(f"Liječnik {ime} {prezime} uspješno izmjenjen!!",
                                "information")
            self.data_changed.emit() #odašiljanje signala o izvršenoj izmjeni
            self.close()
            
        except Exception as exc:
            self.porukaProzor(str(exc))


    #metoda za ispis poruke -- po defaultu pojavljuje se kao upozorenje osim ako ne primi drugačji argument
    def porukaProzor(self,poruka:str,upozorenje="warning"):                     
        porukaProzor = QMessageBox()
        if upozorenje=="warning":
            porukaProzor.setIcon(QMessageBox.Warning)
        else:
            porukaProzor.setIcon(QMessageBox.Information)
        porukaProzor.setText(poruka)
        porukaProzor.setStandardButtons(QMessageBox.Ok)

        porukaProzor.exec()