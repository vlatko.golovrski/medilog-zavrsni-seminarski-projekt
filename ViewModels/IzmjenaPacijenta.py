"""Izmjena pacijenta prima podatke o odabranom pacijentu od PopisPacijenata.py i popunjava polja sa odgovarajućim podacima. 
Korisnik može mijenjati podatke o pacijentu ili dopisivati postojeću dijagnozu odnosno povijest bolesti. Nakon 
što korisnik klikne na SPREMI gumb pokreće se metoda koja uz pomoć sesije sprema izmjenjene podatke u bazu i obavijest o
uspješno spremljenim izmjenama."""



from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5 import uic
from Models.Pacijent import Pacijent
from Models.Lijecnik import Lijecnik

'''
@author: Vlatko Golovrški
@version: 1.0
@date of creation: 20.4.2023.

'''


class IzmjenaPacijenta(QWidget):
    data_changed = pyqtSignal() #stvaranje klase signala u PYQt-u kako bi se mogla pozvati iz metode spremi izmjene
    
    def __init__(self,ime, prezime, oib, adresa, datumRodenja,
                  spol, brOsigurane, telefon, email, odabraniLijecnik, sesija, povratak):
        super().__init__()
        uic.loadUi("./Views/izmjenaPacijenta.ui",self)
        self.sesija = sesija
        self.povratak = povratak

        self.ime_text.setText(ime)
        self.prezime_text.setText(prezime)
        self.oib_text.setText(oib)
        self.adresa_text.setText(adresa)
        self.datum_text.setText(datumRodenja)
        self.spol_combo.setCurrentText(spol)
        self.brojOsigurane_text.setText(brOsigurane)
        self.telefon_text.setText(telefon)
        self.email_text.setText(email)

        #popunjavanje combo box-a imenima i prezimenima liječnika iz baze kako bi se mogao promijeniti
        #dodjeljenji liječnik
        self.odabirDoktora_combo.clear()
        lijecnici = self.sesija.query(Lijecnik).all()
        for lijecnik in lijecnici:
            imePrezimeLijecnika = f"{lijecnik.IME} {lijecnik.PREZIME}"
            doctor_id = lijecnik.ID
            self.odabirDoktora_combo.addItem(imePrezimeLijecnika, userData=doctor_id) # imenima i prezimenima se popunjava combobox a programu su vidljivs ID-ovi liječnika

        
        self.odabirDoktora_combo.setCurrentText(odabraniLijecnik)       
        
        #izvlačenje povijesti bolesti i dijagnoze za pacijenta koji se mijenja
        povijestBolesti = self.sesija.query(Pacijent.POVIJESTBOLESTI).filter(Pacijent.OIB == oib).all()
        dijagnoza = self.sesija.query(Pacijent.DIJAGNOZA).filter(Pacijent.OIB == oib).all()
        
        #popunjavanje tekstualnih okvira povijesti bolesti i dijagnoze dosadašnjim unosima
        self.PovijestBolesti_text.setText('\n'.join(str(item[0]) for item in povijestBolesti))
        self.Dijagnoza_text.setText('\n'.join(str(item[0]) for item in dijagnoza))

        self.izlazButton.clicked.connect(self.close)
        self.izlazButton.clicked.connect(povratak)
        self.SpremiButton.clicked.connect(self.spremiIzmjene)
        self.SpremiButton.clicked.connect(povratak)

    
    def spremiIzmjene(self):
        ime = self.ime_text.text().strip()
        prezime = self.prezime_text.text().strip()
        oib = self.oib_text.text().strip()
        adresa = self.adresa_text.text().strip()
        datumRodjenja = self.datum_text.text().strip()
        spol = self.spol_combo.currentText()
        brOsigurane = self.brojOsigurane_text.text().strip()
        telefon = self.telefon_text.text().strip()
        email = self.email_text.text().strip()
        odabraniLijecnikId = self.odabirDoktora_combo.currentData(Qt.UserRole)
        povijestBolesti = self.PovijestBolesti_text.toPlainText()
        dijagnoza = self.Dijagnoza_text.toPlainText()


        try:
            pacijent = self.sesija.query(Pacijent).filter(Pacijent.OIB == oib).first()

            pacijent.IME = ime
            pacijent.PREZIME = prezime
            pacijent.OIB = oib
            pacijent.ADRESA = adresa
            pacijent.DATUMRODENJA = datumRodjenja
            pacijent.SPOL = spol
            pacijent.BROJOSIGURANEOSOBE = brOsigurane
            pacijent.TELEFON = telefon
            pacijent.EMAIL = email
            pacijent.ODABIRDOKTORA = odabraniLijecnikId
            pacijent.POVIJESTBOLESTI = povijestBolesti
            pacijent.DIJAGNOZA = dijagnoza

            self.sesija.commit()
                                
            self.porukaProzor(f"Pacijent {ime} {prezime} uspješno izmjenjen!!",
                                "information")
            self.data_changed.emit() #odašiljanje signala o izvršenoj izmjeni
            self.close()
            

        except Exception as exc:
            self.porukaProzor(str(exc))

    #metoda za ispis poruke -- po defaultu pojavljuje se kao upozorenje osim ako ne primi drugačji argument
    def porukaProzor(self,poruka:str,upozorenje="warning"):                     
        porukaProzor = QMessageBox()
        if upozorenje=="warning":
            porukaProzor.setIcon(QMessageBox.Warning)
        else:
            porukaProzor.setIcon(QMessageBox.Information)
        porukaProzor.setText(poruka)
        porukaProzor.setStandardButtons(QMessageBox.Ok)

        porukaProzor.exec()