"""Početni ekran programa koji prikazuje polje za unos lozinke i gumb za prijavu. Nakon što korisnik
unese lozinku provjerava se ispravnost lozinke. Ako lozinka nije ispravna prikazuje se poruka o neispravnoj
lozinki te se briše korisnikov unos. Ako je lozinka točna prozor se zatvara i poziva se glavni prozor programa."""


from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5 import uic
from ViewModels.GlavniProzor import GlavniProzor

'''
@author: Vlatko Golovrški
@version: 1.0
@date of creation: 20.4.2023.

'''

class LoginProzor(QMainWindow):
    def __init__(self,sesija):
        super().__init__()
        
        self.sesija = sesija

        uic.loadUi("./Views/loginProzor.ui",self)
        self.lozinkaTekst.setEchoMode(QLineEdit.Password) #asterisk sakrivanje upisane lozinke
        self.lozinkaTekst.returnPressed.connect(self.Prijava) #mogućnost unosa enterom
        self.prijavaButton.clicked.connect(self.Prijava)
    

    def Prijava(self):
        unosKorisnika = self.lozinkaTekst.text().strip()

        if unosKorisnika != "administrator":
           porukaProzor = QMessageBox()
           porukaProzor.setIcon(QMessageBox.Warning) 
           porukaProzor.setText("Pogrešna lozinka!")
           porukaProzor.setStandardButtons(QMessageBox.Ok)
           porukaProzor.exec()
           self.lozinkaTekst.setText("")
        else:
            self.GlavniProzor = GlavniProzor(self.sesija)
            self.GlavniProzor.show()
            self.close()
