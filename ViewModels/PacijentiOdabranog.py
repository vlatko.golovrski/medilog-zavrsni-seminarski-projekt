"""PacijentiOdabranog.py otvara prozor za prikaz pacijenata koji su dodjeljeni odabranom liječniku. Na osnovi ID liječnika koji je dobio od
popisa liječnika sesijom se popunjava tablica sa pacijentima koji u svojoj kolumni dodjeljenog liječnika imaju ID liječinka koji je odabran.
Iznad tablice stvara se naslov s odgovarajućim imenom i prezimenom liječnika. 
Korisnik ima mogućnost brisanja ili izmjene odabranog pacijenta i povratak na prethodni prozor. 
Metoda za brisanje pacijenta prvo postavlja pitanje korisniku da li je siguran a nakon toga pokreće sesiju za brisanje odabranog
pacijenta iz baze podataka. 
Metoda za izmjenu pacijenta prikuplja podatke o odabranom pacijentu i otvara novi prozor za izmjenu pacijenta. Po povratku sa izmjene prima signal 
da je izmjena izvršena te pokreće metodu za osvježavanje tablice. """


from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5 import uic
from ViewModels.IzmjenaPacijenta import IzmjenaPacijenta
from Models.Lijecnik import Lijecnik
from Models.Pacijent import Pacijent

'''
@author: Vlatko Golovrški
@version: 1.0
@date of creation: 20.4.2023.

'''


class PacijentiOdabranog(QWidget):
    def __init__(self, IdOdabranogLijecnika,sesija, povratak):
        super().__init__()
        uic.loadUi("./Views/pacijentiOdabranog.ui",self)
        self.sesija = sesija

        self.IdOdabranogLijecnika = IdOdabranogLijecnika
        self.izlazButton.clicked.connect(self.close)
        self.izlazButton.clicked.connect(povratak)
        self.brisanjeButton.clicked.connect(self.brisanjePacijenta)
        self.izmjenaButton.clicked.connect(self.izmjenaPacijenta) 
        
        #vađenje imena i prezimena liječnika iz baze kako bi se prepravila labela na vrhu prozora
        odabraniLijecnik = self.sesija.query(Lijecnik).filter_by(ID=self.IdOdabranogLijecnika).first()
        self.ime = odabraniLijecnik.IME
        self.prezime = odabraniLijecnik.PREZIME        
        self.pacijentiLijecnikaLabel.setText(f"Pacijenti liječnika {self.ime} {self.prezime}")
        self.pacijentiLijecnikaLabel.setStyleSheet("QLabel { color: rgb(203, 203, 203); }")

        #sesija u kojoj se vade informacije samo o pacijentima kojima je u kolumni ODABIRDOKTORA pronađen ID liječnika čiji se pacijenti prikazuju
        redovi = self.sesija.query(
            Pacijent.IME, Pacijent.PREZIME, Pacijent.OIB, Pacijent.ADRESA,
            Pacijent.DATUMRODENJA, Pacijent.SPOL, Pacijent.BROJOSIGURANEOSOBE,
            Pacijent.TELEFON, Pacijent.EMAIL, Lijecnik.IME, Lijecnik.PREZIME
        ).join(
            Lijecnik, Pacijent.ODABIRDOKTORA == Lijecnik.ID
        ).filter(
            Pacijent.ODABIRDOKTORA == self.IdOdabranogLijecnika
        ).all()

        #popunjavanje tablice podacima o pacijentima traženog liječnika
        self.tablica.setRowCount(len(redovi))
        trenutniRedak = 0
        for redak in redovi:
            self.tablica.setItem(trenutniRedak,0,QTableWidgetItem(redak[0]))
            self.tablica.setItem(trenutniRedak,1,QTableWidgetItem(redak[1]))
            self.tablica.setItem(trenutniRedak,2,QTableWidgetItem(redak[2]))
            self.tablica.setItem(trenutniRedak,3,QTableWidgetItem(redak[3]))
            self.tablica.setItem(trenutniRedak,4,QTableWidgetItem(redak[4]))
            self.tablica.setItem(trenutniRedak,5,QTableWidgetItem(redak[5]))
            self.tablica.setItem(trenutniRedak,6,QTableWidgetItem(redak[6]))
            self.tablica.setItem(trenutniRedak,7,QTableWidgetItem(redak[7]))
            self.tablica.setItem(trenutniRedak,8,QTableWidgetItem(redak[8]))
            self.tablica.setItem(trenutniRedak,9,QTableWidgetItem(redak[9]+" "+redak[10]))
            
            trenutniRedak += 1

        self.tablica.setCurrentCell(0, -1) #stavljanje trenutno odabranog reda izvan prikazane tablice    

    def brisanjePacijenta(self):
        odabraniRed = self.tablica.currentRow()
        if odabraniRed < 0:
            return
        
        oib = self.tablica.item(odabraniRed, 2).text()
        ime = self.tablica.item(odabraniRed, 0).text()
        prezime = self.tablica.item(odabraniRed, 1).text()
        
        odgovor = QMessageBox(self)
        odgovor.setWindowTitle("Potvrda brisanja")
        odgovor.setText(f"Jeste li sigurni da želite izbrisati pacijenta {ime} {prezime}?")
        odgovor.setIcon(QMessageBox.Question)
        odgovor.setStandardButtons(QMessageBox.Yes | QMessageBox.No)
        
        yesButton = odgovor.button(QMessageBox.Yes)
        yesButton.setStyleSheet("background-color: rgb(203, 203, 203);")

        noButton = odgovor.button(QMessageBox.No)
        noButton.setStyleSheet("background-color: rgb(203, 203, 203);")
        
        odgovor.setStyleSheet("QLabel { color: rgb(203, 203, 203); }")

        response = odgovor.exec_()

        if response == QMessageBox.No:
            return
        #brisanje pacijenta
        pacijentZaBrisanje = self.sesija.query(Pacijent).filter(Pacijent.OIB == oib).first()
        self.sesija.delete(pacijentZaBrisanje)
        self.sesija.commit()
        
        self.tablica.removeRow(odabraniRed)

    def izmjenaPacijenta(self):
        odabraniRed = self.tablica.currentRow()
        if odabraniRed < 0:
            return
        
        
        ime = self.tablica.item(odabraniRed, 0).text()
        prezime = self.tablica.item(odabraniRed, 1).text()
        oib = self.tablica.item(odabraniRed, 2).text()
        adresa = self.tablica.item(odabraniRed, 3).text()
        datumRodenja = self.tablica.item(odabraniRed, 4).text()
        spol = self.tablica.item(odabraniRed, 5).text()
        brOsigurane = self.tablica.item(odabraniRed, 6).text()
        telefon = self.tablica.item(odabraniRed, 7).text()
        email = self.tablica.item(odabraniRed, 8).text()
        odabraniLijecnik = self.tablica.item(odabraniRed, 9).text()
        
        

        self.prozorIzmjenaPacijenta = IzmjenaPacijenta(ime, prezime, oib, adresa, datumRodenja,
                                                        spol, brOsigurane, telefon, email,
                                                        odabraniLijecnik, self.sesija, self.show)
        self.prozorIzmjenaPacijenta.show()
        self.close()
        self.prozorIzmjenaPacijenta.data_changed.connect(self.refresh_table)
    
    def refresh_table(self):
        redovi = self.sesija.query(
            Pacijent.IME, Pacijent.PREZIME, Pacijent.OIB, Pacijent.ADRESA,
            Pacijent.DATUMRODENJA, Pacijent.SPOL, Pacijent.BROJOSIGURANEOSOBE,
            Pacijent.TELEFON, Pacijent.EMAIL, Lijecnik.IME, Lijecnik.PREZIME
        ).join(
            Lijecnik, Pacijent.ODABIRDOKTORA == Lijecnik.ID
        ).filter(
            Pacijent.ODABIRDOKTORA == self.IdOdabranogLijecnika
        ).all()
        self.tablica.setRowCount(len(redovi))
        trenutniRedak = 0
        for redak in redovi:
            self.tablica.setItem(trenutniRedak,0,QTableWidgetItem(redak[0]))
            self.tablica.setItem(trenutniRedak,1,QTableWidgetItem(redak[1]))
            self.tablica.setItem(trenutniRedak,2,QTableWidgetItem(redak[2]))
            self.tablica.setItem(trenutniRedak,3,QTableWidgetItem(redak[3]))
            self.tablica.setItem(trenutniRedak,4,QTableWidgetItem(redak[4]))
            self.tablica.setItem(trenutniRedak,5,QTableWidgetItem(redak[5]))
            self.tablica.setItem(trenutniRedak,6,QTableWidgetItem(redak[6]))
            self.tablica.setItem(trenutniRedak,7,QTableWidgetItem(redak[7]))
            self.tablica.setItem(trenutniRedak,8,QTableWidgetItem(redak[8]))
            self.tablica.setItem(trenutniRedak,9,QTableWidgetItem(redak[9]+" "+redak[10]))
            
            trenutniRedak += 1