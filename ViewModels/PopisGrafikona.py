"""PopisGrafikona.py daje korisniku mogućnost pregleda tri grafikona iz unesenih podataka. 
Svaki gumb pokreće zasebnu metodu koja povlači relevantne podatke iz baze podataka i stvara odgovarajući
grafički prikaz koristeći Matplotlib biblioteku te sprema graf u posebnu JPG datoteku koja se učitava u 
sljedećem prozoru koji se otvara nakon toga. """


from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5 import uic
from Models.Lijecnik import Lijecnik
from Models.Pacijent import Pacijent
from ViewModels.PrikazGrafikona import PrikazGrafikona
from sqlalchemy import func
import matplotlib.pyplot as plt


'''
@author: Vlatko Golovrški
@version: 1.0
@date of creation: 20.4.2023.

'''

class PopisGrafikona(QWidget):
    def __init__(self,sesija, povratak):
        super().__init__()
        uic.loadUi("./Views/popisGrafikona.ui",self)
        self.sesija = sesija

        self.povratakButton.clicked.connect(self.close)
        self.povratakButton.clicked.connect(povratak)

        self.muskiZenskiPacijentiButton.clicked.connect(self.muskiZenskiPacijenti)
        self.muskiZenskiLijecniciButton.clicked.connect(self.muskiZenskiLijecnici)
        self.brojPacijenataPoLijecnikuButton.clicked.connect(self.brojPacijenataPoLijecniku)


    
    def muskiZenskiPacijenti(self):
        #prebrojavanje i grupiranje pacijenata po spolu 
        podaci = self.sesija.query( Pacijent.SPOL, func.count(Pacijent.ID)).group_by(Pacijent.SPOL).all()
        #iz liste podaci izvlače se dva spola i pripadajući brojači
        spol = [item[0] for item in podaci]
        brojac = [item[1] for item in podaci]

        colors = ['#66b3ff','#ff9999']
        plt.bar(spol, brojac, color = colors)

        plt.title('Odnos muških i ženskih pacijenata')
        plt.xlabel('SPOL')
        plt.ylabel('BROJ')
   
        #postavljanje brojeva na vrh stupaca
        for i, v in enumerate(brojac):
            plt.text(i, v + -0.5, str(v), color='black', fontweight='bold', ha='center')

        plt.xticks(spol)
        plt.ylim(0, max(brojac) + 1)
        plt.yticks(range(max(brojac) + 1))   
        plt.savefig("./Views/grafikon.jpg", dpi=300)

        self.prozorGrafikon = PrikazGrafikona(self.show)
        self.prozorGrafikon.show()
        self.close()
        plt.clf()



    def muskiZenskiLijecnici(self):
        #prebrojavanje i grupiranje liječnika po spoli
        podaci = self.sesija.query( Lijecnik.SPOL, func.count(Lijecnik.ID)).group_by(Lijecnik.SPOL).offset(1).all()
        #iz liste podaci izvlače se dva spola i pripadajući brojači
        spol = [item[0] for item in podaci]
        brojac = [item[1] for item in podaci]

        colors = ['#66b3ff','#ff9999']
        plt.bar(spol, brojac, color = colors)

        plt.title('Odnos muških i ženskih liječnika')
        plt.xlabel('SPOL')
        plt.ylabel('BROJ')
   
        #postavljanje brojeva na vrh stupaca
        for i, v in enumerate(brojac):
            plt.text(i, v + -0.5, str(v), color='black', fontweight='bold', ha='center')

        plt.xticks(spol)
        plt.ylim(0, max(brojac) + 1)
        plt.yticks(range(max(brojac) + 1))   
        plt.savefig("./Views/grafikon.jpg", dpi=300)

        self.prozorGrafikon = PrikazGrafikona(self.show)
        self.prozorGrafikon.show()
        self.close()
        plt.clf()
        
    def brojPacijenataPoLijecniku(self):
        #iz obiju tablica izvlače se adrese svih pacijenata i odgovarajućih odnosno njima dodjeljenih liječnika
        spojeniPodaci = self.sesija.query(Pacijent, Lijecnik).\
                    filter(Pacijent.ODABIRDOKTORA == Lijecnik.ID).all()

        brojPacijenataPoLijecniku = {} #riječnik u koji ćemo ubacivati liječnike i broj njihovih pacijenata

        #program prolazi kroz svaki tuple u listi spojeniPodaci i prvo stvara varijablu imeLijecnika iz
        #atributa IME i PREZIME klase Lijecnik
        for pacijent, lijecnik in spojeniPodaci:
            imeLijecnika = f"{lijecnik.IME} {lijecnik.PREZIME}"
            #ovdje se provjerava da li već postoji imeLijecnika u rječniku te ako postoji povećava se broj pacijenata
            #za 1, ako nema stvara se novi ključ u riječniku s vrijednošću 1
            if imeLijecnika in brojPacijenataPoLijecniku:
                brojPacijenataPoLijecniku[imeLijecnika] += 1
            else:
                brojPacijenataPoLijecniku[imeLijecnika] = 1

        #kad imamo riječnik sa imenima liječnika i brojem njima dodjeljenih pacijenata pravimo popis za 
        #prikaz u grafikonu
        x = list(brojPacijenataPoLijecniku.keys())
        y = list(brojPacijenataPoLijecniku.values())

        plt.bar(x, y)

        plt.title("Broj pacijenata po liječniku")
        plt.xlabel("Liječnik")
        plt.ylabel("Broj pacijenata")
        plt.xticks(rotation=15, ha="right")
        
        #postavljanje brojeva na vrh stupaca
        for i, v in enumerate(y):
            plt.text(i, v + -0.5, str(v), color='black', fontweight='bold', ha='center')

        plt.xticks(x) #oznake na x osi
        plt.ylim(0, max(y) + 1) #limit y osi od 0 do maksimalne vrijednosti y +1
        plt.yticks(range(max(y) + 1))   #oznake na y osi su od 0 do maksimalne vrijednosti y +1

        plt.savefig("./Views/grafikon.jpg", dpi=300)
        
        
        self.prozorGrafikon = PrikazGrafikona(self.show)
        self.prozorGrafikon.show()
        self.close()
        plt.clf()
      
