"""PopisLijecnika.py nakon učitavanja prozora za prikaz popisa liječnika koristi sesiju kako bi povukao podatke o liječnicima iz baze 
podataka te ih zatim popunjava u tablicu. Korisnik ima opcije povratka na prethodni prozor, brisanje odabranog liječnika, izmjenu odabranog 
liječnika te prikaz pacijenata koji su dodjeljeni odabranom liječniku. 
Brisanje liječnika obavlja metoda brisanjeLijecnika tako da prvo postavlja pitanje korisniku da li je siguran a zatim uz pomoć OIB-a i sesije briše
odabranog liječnika iz baze podataka. Također svim pacijentima koji su bili dodjeljeni tom liječniku mijenja se kolumna dodjeljenog liječnika
u vrijednost 0 što je u tablici liječnika vrijednost pod nazivom "Pacijent bez dodjeljenog liječnika". 
Izmjena liječnika prikuplja podatke o odabranom liječniku i šalje ih u novi prozor za izmjenu liječnika. Nakon što primi signal od prozora za izmjenu
da je izmjena izvršena pokreće se osvježavanje tablice.
Metoda pacijentiOdabranog otvara novi prozor za prikaz pacijenata koji su dodjeljeni liječniku i šalje mu ID liječnika čiji će se pacijenti prikazivati."""


from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5 import uic
from ViewModels.IzmjenaLijecnika import IzmjenaLijecnika
from ViewModels.PacijentiOdabranog import PacijentiOdabranog
from Models.Lijecnik import Lijecnik
from Models.Pacijent import Pacijent

'''
@author: Vlatko Golovrški
@version: 1.0
@date of creation: 20.4.2023.

'''


class PopisLijecnika(QWidget):
    def __init__(self,sesija, povratak):
        super().__init__()
        uic.loadUi("./Views/popisLijecnika.ui",self)
        self.sesija = sesija

        redovi = self.sesija.query(Lijecnik.IME, Lijecnik.PREZIME, Lijecnik.OIB,
                                Lijecnik.ADRESA, Lijecnik.DATUMRODENJA, Lijecnik.SPOL,
                                Lijecnik.SPECIJALIZACIJA, Lijecnik.REGBROJ, Lijecnik.TELEFON,
                                Lijecnik.EMAIL).all()
        self.tablica.setRowCount(len(redovi)-1)
        trenutniRedak = -1
        for redak in redovi:
            self.tablica.setItem(trenutniRedak,0,QTableWidgetItem(redak[0]))
            self.tablica.setItem(trenutniRedak,1,QTableWidgetItem(redak[1]))
            self.tablica.setItem(trenutniRedak,2,QTableWidgetItem(redak[2]))
            self.tablica.setItem(trenutniRedak,3,QTableWidgetItem(redak[3]))
            self.tablica.setItem(trenutniRedak,4,QTableWidgetItem(redak[4]))
            self.tablica.setItem(trenutniRedak,5,QTableWidgetItem(redak[5]))
            self.tablica.setItem(trenutniRedak,6,QTableWidgetItem(redak[6]))
            self.tablica.setItem(trenutniRedak,7,QTableWidgetItem(redak[7]))
            self.tablica.setItem(trenutniRedak,8,QTableWidgetItem(redak[8]))
            self.tablica.setItem(trenutniRedak,9,QTableWidgetItem(redak[9]))
            trenutniRedak += 1
        
        self.tablica.setCurrentCell(0, -1)
   
        self.izlazButton.clicked.connect(self.close)
        self.izlazButton.clicked.connect(povratak)
        self.brisanjeButton.clicked.connect(self.brisanjeLijecnika)
        self.izmjenaButton.clicked.connect(self.izmjenaLijecnika)
        self.pacijentiOdabranogButton.clicked.connect(self.pacijentiOdabranog)
        
        
        
    def brisanjeLijecnika(self):
        
        odabraniRed = self.tablica.currentRow()
        if odabraniRed < 0:
            return
        
        oib = self.tablica.item(odabraniRed, 2).text()
        ime = self.tablica.item(odabraniRed, 0).text()
        prezime = self.tablica.item(odabraniRed, 1).text()
        
        odgovor = QMessageBox(self)
        odgovor.setWindowTitle("Potvrda brisanja")
        odgovor.setText(f"Jeste li sigurni da želite izbrisati liječnika {ime} {prezime}?")
        odgovor.setIcon(QMessageBox.Question)
        odgovor.setStandardButtons(QMessageBox.Yes | QMessageBox.No)

        yesButton = odgovor.button(QMessageBox.Yes)
        yesButton.setStyleSheet("background-color: rgb(203, 203, 203);")

        noButton = odgovor.button(QMessageBox.No)
        noButton.setStyleSheet("background-color: rgb(203, 203, 203);")
        
        odgovor.setStyleSheet("QLabel { color: rgb(203, 203, 203); }")
        response = odgovor.exec_()

        if response == QMessageBox.No:
            return

        #na temelju OIB-a odabranog liječnika izvlači se unos liječnika iz baze koji će se brisati        
        lijecnikKojiSeBrise = self.sesija.query(Lijecnik).filter_by(OIB=oib).first()

        if lijecnikKojiSeBrise:
        
            self.sesija.delete(lijecnikKojiSeBrise)
            self.sesija.query(Pacijent).filter_by(ODABIRDOKTORA=lijecnikKojiSeBrise.ID).update({Pacijent.ODABIRDOKTORA: 0})
            self.sesija.commit() 

        self.tablica.removeRow(odabraniRed)

    def izmjenaLijecnika(self):
        odabraniRed = self.tablica.currentRow()
        if odabraniRed < 0:
            return
        
        #prikupljanje podataka liječnika koji se mijenja i slanje podataka u klasu IzmjenaLiječnika 
        ime = self.tablica.item(odabraniRed, 0).text()
        prezime = self.tablica.item(odabraniRed, 1).text()
        oib = self.tablica.item(odabraniRed, 2).text()
        adresa = self.tablica.item(odabraniRed, 3).text()
        datumRodenja = self.tablica.item(odabraniRed, 4).text()
        spol = self.tablica.item(odabraniRed, 5).text()
        specijalizacija = self.tablica.item(odabraniRed, 6).text()
        regBroj = self.tablica.item(odabraniRed, 7).text()
        telefon = self.tablica.item(odabraniRed, 8).text()
        email = self.tablica.item(odabraniRed, 9).text()
 
        self.prozorOdabraniLijecnik = IzmjenaLijecnika(ime, prezime, oib, adresa, datumRodenja,
                                                        spol, specijalizacija, regBroj, telefon,
                                                          email, self.sesija, self.show)
        self.prozorOdabraniLijecnik.show()
        self.close()
        self.prozorOdabraniLijecnik.data_changed.connect(self.refresh_table) #pokretanje osvježavanja tablice nakon primanja signala o izvršenoj promjeni

    def refresh_table(self):

        redovi = self.sesija.query(Lijecnik.IME, Lijecnik.PREZIME, Lijecnik.OIB,
                                Lijecnik.ADRESA, Lijecnik.DATUMRODENJA, Lijecnik.SPOL,
                                Lijecnik.SPECIJALIZACIJA, Lijecnik.REGBROJ, Lijecnik.TELEFON,
                                Lijecnik.EMAIL).all()
        self.tablica.setRowCount(len(redovi)-1)
        trenutniRedak = -1
        for redak in redovi:
            self.tablica.setItem(trenutniRedak,0,QTableWidgetItem(redak[0]))
            self.tablica.setItem(trenutniRedak,1,QTableWidgetItem(redak[1]))
            self.tablica.setItem(trenutniRedak,2,QTableWidgetItem(redak[2]))
            self.tablica.setItem(trenutniRedak,3,QTableWidgetItem(redak[3]))
            self.tablica.setItem(trenutniRedak,4,QTableWidgetItem(redak[4]))
            self.tablica.setItem(trenutniRedak,5,QTableWidgetItem(redak[5]))
            self.tablica.setItem(trenutniRedak,6,QTableWidgetItem(redak[6]))
            self.tablica.setItem(trenutniRedak,7,QTableWidgetItem(redak[7]))
            self.tablica.setItem(trenutniRedak,8,QTableWidgetItem(redak[8]))
            self.tablica.setItem(trenutniRedak,9,QTableWidgetItem(redak[9]))
            trenutniRedak += 1
  
    def pacijentiOdabranog(self):
        odabraniRed = self.tablica.currentRow()
        if odabraniRed < 0:
            return
 
        oib = self.tablica.item(odabraniRed, 2).text()
        #na osnovi OIB-a izvlačenje odabranog liječnika iz baze
        odabraniLijecnik = self.sesija.query(Lijecnik).filter_by(OIB=oib).first()
        if not odabraniLijecnik:
            return

        idOdabranogLijecnika = odabraniLijecnik.ID #iz odabranog liječnika vadimo njegov ID i šaljemo u klasu PacijentiOdabranog

        self.prozorOdabraniLijecnik = PacijentiOdabranog(idOdabranogLijecnika, self.sesija, self.show)
        self.prozorOdabraniLijecnik.show()
        self.close()
        
        