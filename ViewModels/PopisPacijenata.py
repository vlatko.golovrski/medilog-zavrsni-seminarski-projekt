""" PopisPacijenata.py prikazuje do sada unesene pacijente u bazu. Nakon učitavanja grafičkog sučeljka postavljena tablica se popunjava podacima iz baze. 
Korisnik ima opciju povratak te brisanje ili izmjenu pacijenta. U slučaju da odabere brisanje pacijenta prvo se postavlja pitanje provjere a ako korisnik odgovori 
potvrdno sesijom se briše odabrani pacijent iz baze podataka. 
Odabirom opcije izmjene pacijenta program prikuplja podatke odabranog pacijenta i šalje ih u novu skriptu pod nazivom IzmjenaPacijenta.py. Nakon povratka sa 
izmjenjenim podacima odnosno nakon što skripta izmjene pacijenta pošalje signal da su podaci promijenjeni pokreće se metoda osvježavanja tablice kako bi se 
vidjele izmjene. """


from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5 import uic
from ViewModels.IzmjenaPacijenta import IzmjenaPacijenta
from Models.Lijecnik import Lijecnik
from Models.Pacijent import Pacijent

'''
@author: Vlatko Golovrški
@version: 1.0
@date of creation: 20.4.2023.

'''

class PopisPacijenata(QWidget):
    def __init__(self,sesija, povratak):
        super().__init__()
        uic.loadUi("./Views/popisPacijenata.ui",self)
        self.sesija = sesija
       
        #popunjavanje tablice pacijentima iz baze podataka
        redovi = self.sesija.query(Pacijent.IME, Pacijent.PREZIME, Pacijent.OIB,
                                    Pacijent.ADRESA, Pacijent.DATUMRODENJA, Pacijent.SPOL,
                                    Pacijent.BROJOSIGURANEOSOBE, Pacijent.TELEFON,
                                    Pacijent.EMAIL, Lijecnik.IME,
                                    Lijecnik.PREZIME).join(Lijecnik, Pacijent.ODABIRDOKTORA == Lijecnik.ID).all()
        self.tablica.setRowCount(len(redovi))
        trenutniRedak = 0
        for redak in redovi:
            self.tablica.setItem(trenutniRedak,0,QTableWidgetItem(redak[0]))
            self.tablica.setItem(trenutniRedak,1,QTableWidgetItem(redak[1]))
            self.tablica.setItem(trenutniRedak,2,QTableWidgetItem(redak[2]))
            self.tablica.setItem(trenutniRedak,3,QTableWidgetItem(redak[3]))
            self.tablica.setItem(trenutniRedak,4,QTableWidgetItem(redak[4]))
            self.tablica.setItem(trenutniRedak,5,QTableWidgetItem(redak[5]))
            self.tablica.setItem(trenutniRedak,6,QTableWidgetItem(redak[6]))
            self.tablica.setItem(trenutniRedak,7,QTableWidgetItem(redak[7]))
            self.tablica.setItem(trenutniRedak,8,QTableWidgetItem(redak[8]))
            self.tablica.setItem(trenutniRedak,9,QTableWidgetItem(redak[9]+" "+redak[10]))
            trenutniRedak += 1
       
    
        self.tablica.setCurrentCell(0, -1) #namještanje trenutno odabranog reda
        
        self.izlazButton.clicked.connect(self.close)
        self.izlazButton.clicked.connect(povratak)

        self.brisanjeButton.clicked.connect(self.brisanjePacijenta)  
        self.izmjenaButton.clicked.connect(self.izmjenaPacijenta)  

    def brisanjePacijenta(self):
        odabraniRed = self.tablica.currentRow()
        if odabraniRed < 0:
            return
        
        oib = self.tablica.item(odabraniRed, 2).text()
        ime = self.tablica.item(odabraniRed, 0).text()
        prezime = self.tablica.item(odabraniRed, 1).text()
        
        odgovor = QMessageBox(self)
        odgovor.setWindowTitle("Potvrda brisanja")
        odgovor.setText(f"Jeste li sigurni da želite izbrisati pacijenta {ime} {prezime}?")
        odgovor.setIcon(QMessageBox.Question)
        odgovor.setStandardButtons(QMessageBox.Yes | QMessageBox.No)
        
        yesButton = odgovor.button(QMessageBox.Yes)
        yesButton.setStyleSheet("background-color: rgb(203, 203, 203);")

        noButton = odgovor.button(QMessageBox.No)
        noButton.setStyleSheet("background-color: rgb(203, 203, 203);")
        
        odgovor.setStyleSheet("QLabel { color: rgb(203, 203, 203); }")

        response = odgovor.exec_()

        if response == QMessageBox.No:
            return
        
        #brisanje pacijenta
        pacijentZaBrisanje = self.sesija.query(Pacijent).filter(Pacijent.OIB == oib).first()
        self.sesija.delete(pacijentZaBrisanje)
        self.sesija.commit()
        
        self.tablica.removeRow(odabraniRed)       


    def izmjenaPacijenta(self):
        odabraniRed = self.tablica.currentRow()
        if odabraniRed < 0:
            return
                
        #prikupljanje podataka odabranog pacijenta iz tablice i slanje u Izmjena pacijenta
        ime = self.tablica.item(odabraniRed, 0).text()
        prezime = self.tablica.item(odabraniRed, 1).text()
        oib = self.tablica.item(odabraniRed, 2).text()
        adresa = self.tablica.item(odabraniRed, 3).text()
        datumRodenja = self.tablica.item(odabraniRed, 4).text()
        spol = self.tablica.item(odabraniRed, 5).text()
        brOsigurane = self.tablica.item(odabraniRed, 6).text()
        telefon = self.tablica.item(odabraniRed, 7).text()
        email = self.tablica.item(odabraniRed, 8).text()
        odabraniLijecnik = self.tablica.item(odabraniRed, 9).text()
        
        self.prozorIzmjenaPacijenta = IzmjenaPacijenta(ime, prezime, oib, adresa, datumRodenja,
                                                        spol, brOsigurane, telefon, email,
                                                        odabraniLijecnik, self.sesija, self.show)
        self.prozorIzmjenaPacijenta.show() 
        self.close()
        self.prozorIzmjenaPacijenta.data_changed.connect(self.refresh_table) #osvježavanje tablice nakon primanja signala o izvršenoj izmjeni

    def refresh_table(self):

        redovi = self.sesija.query(Pacijent.IME, Pacijent.PREZIME, Pacijent.OIB,
                                    Pacijent.ADRESA, Pacijent.DATUMRODENJA, Pacijent.SPOL,
                                    Pacijent.BROJOSIGURANEOSOBE, Pacijent.TELEFON,
                                    Pacijent.EMAIL, Lijecnik.IME,
                                    Lijecnik.PREZIME).join(Lijecnik, Pacijent.ODABIRDOKTORA == Lijecnik.ID).all()
        self.tablica.setRowCount(len(redovi))
        trenutniRedak = 0
        for redak in redovi:
            self.tablica.setItem(trenutniRedak,0,QTableWidgetItem(redak[0]))
            self.tablica.setItem(trenutniRedak,1,QTableWidgetItem(redak[1]))
            self.tablica.setItem(trenutniRedak,2,QTableWidgetItem(redak[2]))
            self.tablica.setItem(trenutniRedak,3,QTableWidgetItem(redak[3]))
            self.tablica.setItem(trenutniRedak,4,QTableWidgetItem(redak[4]))
            self.tablica.setItem(trenutniRedak,5,QTableWidgetItem(redak[5]))
            self.tablica.setItem(trenutniRedak,6,QTableWidgetItem(redak[6]))
            self.tablica.setItem(trenutniRedak,7,QTableWidgetItem(redak[7]))
            self.tablica.setItem(trenutniRedak,8,QTableWidgetItem(redak[8]))
            self.tablica.setItem(trenutniRedak,9,QTableWidgetItem(redak[9]+" "+redak[10]))
            trenutniRedak += 1
      
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
       
   