"""PrikazGrafikona.py učitava posebni prozor koji učitava prethodno stvorenu jpg datoteku odnosno grafikon stvoren u PopisGrafikona.py. 
Korisnik ima samo jednu opciju za povratak. Nakon klikanja na gumb povratak skripta briše stvorenu jpg datoteku. """


from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5 import uic
import os

'''
@author: Vlatko Golovrški
@version: 1.0
@date of creation: 20.4.2023.

'''


class PrikazGrafikona(QWidget):
    def __init__(self, povratak):
        super().__init__()
        uic.loadUi("./Views/prikazGrafikona.ui",self)


        self.povratakButton.clicked.connect(self.close)
        self.povratakButton.clicked.connect(povratak)
        self.povratakButton.clicked.connect(lambda: os.remove("./Views/grafikon.jpg"))
