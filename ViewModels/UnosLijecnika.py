"""UnosLijecnika.py prikazuje prozor sa poljima za unos podataka o novom lijecniku. Korisnik popunjava polja sa odgovarajućim podacima. 
Program korisniku nudi mogućnost povratka na prijašnji prozor, brisanje podataka iz unesenih polja ili spremanje novog lijecnika. Ako korisnik 
pritisne gumb SPREMI program provjerava točnost podataka u smislu da OIB mora biti sastavljen od 11 znamenaka te samo od brojeva, registracijski broj liječnika 
mora biti 8 znamenaka isto samo od brojeva. Program također prije snimanja provjerava da li u bazi podataka već postiji liječnik sa identičnim OIB-om
ili registracijskim brojem. Tek kad su se svi uvjeti zadovoljili program snima liječnika u bazu i briše polja za unos kako bi bila spremna za
unos sljedećeg liječnika."""


from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5 import uic
from Models.Lijecnik import Lijecnik

'''
@author: Vlatko Golovrški
@version: 1.0
@date of creation: 20.4.2023.

'''


class UnosLijecnika(QWidget):
    def __init__(self,sesija, povratak) :
        super().__init__()
        uic.loadUi("./Views/unosLijecnika.ui",self)
        self.sesija = sesija

        self.izlazButton.clicked.connect(self.close)
        self.izlazButton.clicked.connect(povratak)
        self.SpremiButton.clicked.connect(self.unosLijecnika)
        self.brisiButton.clicked.connect(self.obrisi)

    def porukaProzor(self,poruka:str,upozorenje="warning"):                     
        porukaProzor = QMessageBox()
        if upozorenje=="warning":
            porukaProzor.setIcon(QMessageBox.Warning)
        else:
            porukaProzor.setIcon(QMessageBox.Information)
        porukaProzor.setText(poruka)
        porukaProzor.setStandardButtons(QMessageBox.Ok)

        porukaProzor.exec()

    def unosLijecnika(self):
        #preuzimanje unesenih podataka u varijable
        ime = self.ime_text.text().strip()
        prezime = self.prezime_text.text().strip()
        oib = self.oib_text.text().strip()
        adresa = self.adresa_text.text().strip()
        datumRodjenja = self.dat_rod_text.text().strip()
        spol = self.spol_combo.currentText()
        specijalizacija = self.specijalizacija_text.text().strip()
        regBroj = self.regbroj_text.text().strip()
        telefon = self.telefon_text.text().strip()
        email = self.e_mail_text.text().strip()

        #provjeravanje da li su OIB i registarski broj sastavljeni samo od brojeva i da li su odgovarajuće duljine
        try:        
            if len(oib) != 11 or not oib.isdigit():
                raise ValueError("OIB mora imati jedanaest znamenaka i sastojati se samo od brojeva.")
        except ValueError as e:
            self.porukaProzor(str(e))
            return
        try:        
            if len(regBroj) != 8 or not regBroj.isdigit():
                raise ValueError("Registracijski broj zdravstvenog djelatnika mora imati 8 znamenaka i sastojati se samo od brojeva.")
        except ValueError as e:
            self.porukaProzor(str(e))
            return
        
        #provjeravanje da li u bazi podataka već postoji liječnik sa istim OIB-om ili registarskim brojem
        oibPostoji = self.sesija.query(Lijecnik).filter(Lijecnik.OIB == oib).first()
        regBrojPostoji = self.sesija.query(Lijecnik).filter(Lijecnik.REGBROJ == regBroj).first()
       
        if regBrojPostoji:
            self.porukaProzor("Postoji već liječnik s tim registarskim brojem!")         
        elif oibPostoji:
            self.porukaProzor("Postoji već liječnik s tim OIB-om!")

        #tek u slučaju da su uneseni podaci ispravni pristupa se snimanju liječnika u bazu podataka
        else:
            try:
                noviLijecnik = Lijecnik(
                    IME = ime,PREZIME = prezime,
                    OIB = oib,ADRESA = adresa,
                    DATUMRODENJA = datumRodjenja,
                    SPOL = spol,
                    SPECIJALIZACIJA = specijalizacija,
                    REGBROJ = regBroj,
                    TELEFON = telefon,
                    EMAIL = email)
                self.sesija.add(noviLijecnik)
                self.sesija.commit()
                
                self.porukaProzor(f"Liječnik {noviLijecnik.IME} {noviLijecnik.PREZIME} uspješno unesen!!","information")
                self.obrisi()
        
            except Exception as exc:
                self.porukaProzor(str(exc))    
    
    #čišćenje obrasca za unos liječnika
    def obrisi(self):
        self.ime_text.setText("")
        self.prezime_text.setText("")
        self.oib_text.setText("")
        self.adresa_text.setText("")
        self.dat_rod_text.setText("")
        self.spol_combo.setCurrentText("")
        self.specijalizacija_text.setText("")
        self.regbroj_text.setText("")
        self.telefon_text.setText("")
        self.e_mail_text.setText("")
        
     