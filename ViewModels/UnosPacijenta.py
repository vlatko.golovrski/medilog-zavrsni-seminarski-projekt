"""UnosPacijenta.py prikazuje prozor sa poljima za unos podataka o novom pacijentu. Korisnik popunjava polja sa odgovarajućim podacima. 
Program korisniku nudi mogućnost povratka na prijašnji prozor, brisanje podataka iz unesenih polja ili spremanje novog pacijenta. Ako korisnik 
pritisne gumb SPREMI program provjerava točnost podataka u smislu da OIB mora biti sastavljen od 11 znamenaka te samo od brojeva, broj osigurane osobe 
mora biti 8 znamenaka te isto samo od brojeva. Program također prije snimanja provjerava da li u bazi podataka već postoji pacijent sa identičnim OIB-om
ili brojem osigurane osobe. Tek kada su svi uvjeti zadovoljeni program sprema pacijenta u bazu podataka i briše polja za unos kako bi bila spremna za
unos sljedećeg pacijenta."""


from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5 import uic
from Models.Pacijent import Pacijent
from Models.Lijecnik import Lijecnik

'''
@author: Vlatko Golovrški
@version: 1.0
@date of creation: 20.4.2023.

'''

class UnosPacijenta(QWidget):
    def __init__(self,sesija, povratak) :
        super().__init__()
        uic.loadUi("./Views/unosPacijenta.ui",self)
        self.sesija = sesija

        self.izlazButton.clicked.connect(self.close)
        self.izlazButton.clicked.connect(povratak)
        self.brisiButton.clicked.connect(self.obrisi)
        self.SpremiButton.clicked.connect(self.unosPacijenta)
            
        #popunjavanje combo box-a imenima i prezimenima liječnika iz baze kako bi se mogao dodijeliti 
        #liječnik pacijentu
        self.odabirDoktora_combo.clear()        
        self.odabirDoktora_combo.addItem("")
        lijecnici = self.sesija.query(Lijecnik).all()
        for lijecnik in lijecnici:
            imePrezimeLijecnika = f"{lijecnik.IME} {lijecnik.PREZIME}"
            doctor_id = lijecnik.ID
            self.odabirDoktora_combo.addItem(imePrezimeLijecnika, userData=doctor_id) #combobox će biti popunjen imenima
            #i prezimenima liječnika a programu ostaju vidljivi ID liječnika
  
        
    def porukaProzor(self,poruka:str,upozorenje="warning"):                     
        porukaProzor = QMessageBox()
        if upozorenje=="warning":
            porukaProzor.setIcon(QMessageBox.Warning)
        else:
            porukaProzor.setIcon(QMessageBox.Information)
        porukaProzor.setText(poruka)
        porukaProzor.setStandardButtons(QMessageBox.Ok)
        porukaProzor.exec()    
    

    def unosPacijenta(self):
        ime = self.ime_text.text().strip()
        prezime = self.prezime_text.text().strip()
        oib = self.oib_text.text().strip()
        adresa = self.adresa_text.text().strip()
        datumRodjenja = self.datum_text.text().strip()
        spol = self.spol_combo.currentText()
        brOsigurane = self.brojOsigurane_text.text().strip()
        telefon = self.telefon_text.text().strip()
        email = self.email_text.text().strip()
        povijestBolesti = self.PovijestBolesti_text.toPlainText().strip()     
        odabraniLijecnikId = self.odabirDoktora_combo.currentData(Qt.UserRole)
        
        #provjeravanje da li su OIB i broj osigurane osobe sastavljeni samo od brojeva i da li su odgovarajuće duljine
        try:        
            if len(oib) != 11 or not oib.isdigit():
                raise ValueError("OIB mora imati jedanaest znamenaka i sastojati se samo od brojeva.")
        except ValueError as e:
            self.porukaProzor(str(e))
            return
        try:        
            if len(brOsigurane) != 8 or not brOsigurane.isdigit():
                raise ValueError("Broj osigurane osobe mora imati 8 znamenaka i sastojati se samo od brojeva.")
        except ValueError as e:
            self.porukaProzor(str(e))
            return
        
        #provjeravanje da li u bazi podataka već postoji pacijent sa istim OIB-om ili brojem osiguranja
        oibPostoji = self.sesija.query(Pacijent).filter(Pacijent.OIB == oib).first()
        brOsiguranePostoji = self.sesija.query(Pacijent).filter(Pacijent.BROJOSIGURANEOSOBE == brOsigurane).first()
       
        if brOsiguranePostoji:
            self.porukaProzor("Postoji već pacijent s tim brojem osiguranja!")         
        elif oibPostoji:
            self.porukaProzor("Postoji već pacijent s tim OIB-om!")
        elif odabraniLijecnikId == None:
            self.porukaProzor("Morate dodijeliti liječnika pacijentu!")    
           
        #tek u slučaju da su uneseni podaci ispravni pristupa se snimanju pacijenta u bazu podataka
        else:
            try:
                noviPacijent = Pacijent(
                    IME = ime,PREZIME = prezime,
                    OIB = oib,ADRESA = adresa,
                    DATUMRODENJA = datumRodjenja,
                    SPOL = spol,
                    BROJOSIGURANEOSOBE = brOsigurane,
                    TELEFON = telefon,
                    EMAIL = email,
                    ODABIRDOKTORA = odabraniLijecnikId,
                    POVIJESTBOLESTI = povijestBolesti)
                self.sesija.add(noviPacijent)
                self.sesija.commit()
                
                self.porukaProzor(f"Pacijent {noviPacijent.IME} {noviPacijent.PREZIME} uspješno unesen!!","information")
                self.obrisi()
            
            except Exception as exc:
                self.porukaProzor(str(exc))      
          
    #Čišćenje obrasca za unos pacijenta
    def obrisi(self):
        self.ime_text.setText("")
        self.prezime_text.setText("")
        self.oib_text.setText("")
        self.adresa_text.setText("")
        self.datum_text.setText("")
        self.spol_combo.setCurrentText("")
        self.brojOsigurane_text.setText("")
        self.telefon_text.setText("")
        self.email_text.setText("")
        self.odabirDoktora_combo.setCurrentText("")
        self.PovijestBolesti_text.setText("")
